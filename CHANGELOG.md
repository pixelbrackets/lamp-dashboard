# Changelog

2020-07-07 Dan Untenzu <mail@pixelbrackets.de>
  * 1.0.0
  * Add README

2012-05-07 Dan Untenzu <mail@pixelbrackets.de>

  * FEATURE Initial release
