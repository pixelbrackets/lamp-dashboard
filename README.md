# LAMP Dashboard

[![Build Status](https://img.shields.io/gitlab/pipeline/pixelbrackets/lamp-dashboard?style=flat-square)](https://gitlab.com/pixelbrackets/lamp-dashboard/pipelines)
[![License](https://img.shields.io/badge/license-gpl--2.0--or--later-blue.svg?style=flat-square)](https://spdx.org/licenses/GPL-2.0-or-later.html)

Index project for local LAMP development machine

![Screenshot](docs/screenshot.png)

- List all other projects on this localhost → Uses [localhost-project-listing](https://gitlab.com/pixelbrackets/localhost-project-listing/)
- Show PHP information
- Manage MySQL databases
- Provide a HTML5 template

## Installation

- Clone the Git repository and point the local webserver to `web` directory

## Source

https://gitlab.com/pixelbrackets/lamp-dashboard/

## License

GNU General Public License version 2 or later

The GNU General Public License can be found at http://www.gnu.org/copyleft/gpl.html.

## Author

Dan Untenzu (<mail@pixelbrackets.de> / [@pixelbrackets](https://pixelbrackets.de))

## Changelog

See [./CHANGELOG.md](CHANGELOG.md)

## Contribution

This script is Open Source, so please use, patch, extend or fork it.
