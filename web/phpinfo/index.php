<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>LAMP Dashboard</title>

		<link rel="stylesheet" href="/bootstrap.css" />
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a class="brand" href="/">LAMP Dashboard</a>
					<div class="nav-collapse">
						<ul class="nav">
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<a href="/adminer/">Adminer</a>
							</li>
							<li class="active">
								<a href="/phpinfo/">phpinfo()</a>
							</li>
							<li>
								<a href="/bootstrap/">HTML5 Bootstrap</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<section id="phpexample">
				<h2>PHP Test</h2>

				<p><?php echo 'Hello World' ?></p>
				<p><?php echo 'Current PHP version is ' . phpversion(); ?></p>
			</section>
			<section id="phpinfo">
				<h2>phpinfo()</h2>

				<iframe src="phpinfo.php" width="100%" height="480"></iframe>
			</section>
		</div>
	</body>
</html>